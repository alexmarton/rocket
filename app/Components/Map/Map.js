import MapView from 'react-native-maps';

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View} from 'react-native';


export default class Map extends Component {


    render() {
        return (

            <View style={styles.container}>

                <MapView
                    style={styles.map}
                    region={{
           latitude: 51.436596,
           longitude: 5.478001,
           latitudeDelta: 0.015,
           longitudeDelta: 0.0121,
         }}/>


            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    button: {

        marginBottom: 60,
        width: 190,
        height: 190,
        padding: 5,
        margin: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 45

    }
});

AppRegistry.registerComponent('Map', () => Map);
