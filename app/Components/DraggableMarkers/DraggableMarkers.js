import React from 'react';
import {MyCustomMarkerView, StyleSheet, View, Dimensions,} from 'react-native';
import MapView from 'react-native-maps';

const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 51.441825;
const LONGITUDE = 5.47424;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
const pinColor = '#000000';
const imag_marker = './app/src/button_ok_green.png';
const highlight = require('../../src/img/offer_logo.png');

function log(eventName, e) {
    console.log(eventName, e.nativeEvent);
}

class MarkerTypes extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            a: {
                latitude: LATITUDE + SPACE,
                longitude: LONGITUDE + SPACE,
            },
            b: {
                latitude: LATITUDE - SPACE,
                longitude: LONGITUDE - SPACE,
            },
            highlight : require('../../src/img/offer_logo.png'),
            positionDefault:true,

        };


    }

    onPressLong(){
        this.setState({
            positionDefault:true
        });

    };


    static defaultProps = {
        positionDefault:true
    };



    render() {
        return (
            <View style={styles.container}>
                <MapView
                    onPanDrag={(e) => log('Stop Draggme arround', e) }
                    provider={this.props.provider}
                    style={styles.map}
                    initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,


          }}
                >
                    <MapView.Marker
                        coordinate={this.state.a}
                        onSelect={(e) => log('onSelect', e) }
                        onDrag={(e) => log('onDrag', e)}
                        onDragStart={(e) => log('onDragStart', e)}
                        onDragEnd={(e) => log('onDragEnd', e)}
                        onPress={(e) => log('onPress', e)}
                        image={this.state.highlight}
                        draggable = {this.state.positionDefault}

                    >

                    </MapView.Marker>
                </MapView>
            </View>
        );
    }
}

MarkerTypes.propTypes = {
    provider: MapView.ProviderPropType,
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    pin: {
        width: 30,
        height: 30
    }
});

module.exports = MarkerTypes;