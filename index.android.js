import React, { Component } from 'react';
import {  AppRegistry,  StyleSheet,  Text,  View} from 'react-native';

 //import Map from './app/Components/Map/Map';
 import Gesture from './app/Components/Gesture/Gesture';
 import DraggableMarkers from './app/Components/DraggableMarkers/DraggableMarkers';


export default class rocket extends Component {
  render() {
    return (
        <View style={styles.container}>
            <DraggableMarkers/>
          {/*<Map/>*/}
          <Gesture/>



        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('rocket', () => rocket);
